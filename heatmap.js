const fs = require('fs')
const PNG = require('node-png').PNG
const ndarray = require('ndarray')
const interp = require('ndarray-linear-interpolate')
const kriging = new require('./kriging.js')()

const X_OFFSET = 2.5
const Y_OFFSET = 4
const LINEAR_SCALE = 0.8

const DB_OFFSET = 60
const DB_SCALE = 255 / 50

const FILENAME = 'group2up'

const BACKGROUND_COLOUR = [0, 0, 0, 0xFF]
const HIGH_COLOUR = [0xFF, 0x00, 0x00, 0xFF]

const IMAGE_HEIGHT = 2500
const IMAGE_WIDTH = IMAGE_HEIGHT

const GRID_WIDTH = 25

let x = ndarray(new Float32Array(GRID_WIDTH * GRID_WIDTH), [GRID_WIDTH, GRID_WIDTH])

let model = "exponential"
let sigma2 = 0.5, alpha = 10

let k = {
  t: [],
  x: [],
  y: []
}

let main = () => {
  let img = new PNG({
    width: IMAGE_HEIGHT,
    height: IMAGE_WIDTH
  })

  for (let a = 0; a < IMAGE_HEIGHT; a++) {
    for (let b = 0; b < IMAGE_WIDTH; b++) {
      setPixel(img, b, a, [0, 0, 0, 0xFF])
    }
  }
  
  let json = JSON.parse(fs.readFileSync(`${FILENAME}.json`, 'utf8'))
  
  json.forEach(element => {
    // data.push({
    //   x: element.x,
    //   y: element.y,
    //   z: element.z = (DB_OFFSET + element.v) * DB_SCALE
    // })
    x.set(X_OFFSET + element.x * LINEAR_SCALE, Y_OFFSET + element.y * LINEAR_SCALE, (DB_OFFSET + element.v) * DB_SCALE)
    k.t.push((DB_OFFSET + element.v) * DB_SCALE)
    k.x.push(X_OFFSET + element.x * LINEAR_SCALE)
    k.y.push(Y_OFFSET + element.y * LINEAR_SCALE)
  })
  
  let fitModel = kriging.train(k.t, k.x, k.y, model, sigma2, alpha)
  // setPixel(img, X_OFFSET + element.x * LINEAR_SCALE, Y_OFFSET + element.y * LINEAR_SCALE, [(DB_OFFSET + element.v) * DB_SCALE, 0, 0, 0xFF])
   
  for(let u = 0.0; u <= GRID_WIDTH; u+=GRID_WIDTH / IMAGE_HEIGHT) {
    // var row = []
    for(var v=0.0; v<=GRID_WIDTH; v+=GRID_WIDTH / IMAGE_HEIGHT) {
      // setPixel(img, Math.round(u * (IMAGE_HEIGHT / GRID_WIDTH)), Math.round(v * (IMAGE_HEIGHT / GRID_WIDTH)), [Math.round(interp(x, u, v)), 0, 0, 0xFF])
      setPixel(img, Math.round(u * (IMAGE_HEIGHT / GRID_WIDTH)), Math.round(v * (IMAGE_HEIGHT / GRID_WIDTH)), [0, Math.max(0, Math.round(kriging.predict(u, v, fitModel))), 0, 0xFF])
      // row.push(Math.round(interp(x, u, v)))
    }
    // console.log(row.join(" "))
  }
  
  json.forEach(element => {
    drawSquare(img, (X_OFFSET + element.x * LINEAR_SCALE) * (IMAGE_HEIGHT / GRID_WIDTH), (Y_OFFSET + element.y * LINEAR_SCALE) * (IMAGE_HEIGHT / GRID_WIDTH), 10, 10, HIGH_COLOUR)
  })

  img.pack().pipe(fs.createWriteStream(`${FILENAME}.png`))
}

let drawSquare = (_img, _x, _y, _w, _h, _v) => {
  for (let x = Math.round(_x - _w / 2); x < _x + _w / 2; x++) {
    for (let y = Math.round(_y - _h / 2); y < _y + _h / 2; y++) {
      setPixel(_img, x, y, _v)
    }
  }
}

let setPixel = (_img, _x, _y, _v) => {
  _img.data[(_img.width * _y + _x) * 4] = _v[0]
  _img.data[(_img.width * _y + _x) * 4 + 1] = _v[1]
  _img.data[(_img.width * _y + _x) * 4 + 2] = _v[2]
  _img.data[(_img.width * _y + _x) * 4 + 3] = _v[3]
}

main()